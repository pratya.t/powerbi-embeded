# generate pbi token
# $url = "https://api.powerbi.com/v1.0/myorg/groups/{groupId}/reports/{reportId}/GenerateToken"
$groupid = "b6205fb0-f37e-4e64-99ff-0a7a84e5d9a5"
$reportid = "6eb3ada7-2128-4ed4-a0ce-bd751ab299fd"
$url = "https://api.powerbi.com/v1.0/myorg/groups/" + $groupid +"/reports/" + $reportid + "/GenerateToken"
$body = "{ 'accessLevel': 'View', }"
$response = Invoke-PowerBIRestMethod -Url $url -Body $body -Method Post
$response

$json = $response | ConvertFrom-Json
$json.token

# export to file
$json.token > token.txt
