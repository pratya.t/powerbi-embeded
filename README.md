# SETUP POWER BI EMBEDDED & EXAMPLE SOURCE CODE

embedsetup (create application with Azure AD & grant consent app) / select "Embed for customers"
https://app.powerbi.com/embedsetup

example code : https://github.com/microsoft/PowerBI-Developer-Samples

### config 
```
AUTHENTICATION_MODE = 'MasterUser'
WORKSPACE_ID = PBI Workspace Id or Report Group
REPORT_ID = PBI report id
TENANT_ID = blank
CLIENT_ID = application / client id
CLIENT_SECRET = blank
SCOPE = ['https://analysis.windows.net/powerbi/api/.default']
AUTHORITY = 'https://login.microsoftonline.com/organizations'
POWER_BI_USER = pbi owner user
POWER_BI_PASS = pbi owner password
```


## *** SETUP POWER BI EMBEDDED & TEST (OLD)

## Step 0
install powerbi cmd in powershell  \
https://docs.microsoft.com/en-us/powershell/power-bi/overview?view=powerbi-ps 

```powershell
Install-Module -Name MicrosoftPowerBIMgmt
```

## Step 1 login powerbi via powershell (*require : os windows, powershell )
```powershell
Login-PowerBI
```

## Step 2 get pbi token (export to file)
run below code
```powershell
# generate pbi token
# $url = "https://api.powerbi.com/v1.0/myorg/groups/{groupId}/reports/{reportId}/GenerateToken"
$groupid = "b6205fb0-f37e-4e64-99ff-0a7a84e5d9a5"
$reportid = "6eb3ada7-2128-4ed4-a0ce-bd751ab299fd"
$url = "https://api.powerbi.com/v1.0/myorg/groups/" + $groupid +"/reports/" + $reportid + "/GenerateToken"
$body = "{ 'accessLevel': 'View', }"
$response = Invoke-PowerBIRestMethod -Url $url -Body $body -Method Post
$response

$json = $response | ConvertFrom-Json
$json.token

# export to file
$json.token > token.txt
```

## Step 3 try embed using pbi embedded playground
https://microsoft.github.io/PowerBI-JavaScript/demo/v2-demo/index.html?noRedirect=true&redirectFromNewPlayground=true


require : \
embed token : generate from powershell, \
embed url : edit reportId, reportGroup in original url, \
report id : from report url

## ref. / meterials
how to get pbi token  & use pbi embedded playground \
https://www.youtube.com/watch?v=4KuyPNtVijo \

Embed Token - Reports GenerateTokenInGroup \
https://docs.microsoft.com/en-us/rest/api/power-bi/embed-token/reports-generate-token-in-group#code-try-0 \

Invoke-PowerBIRestMethod \
https://docs.microsoft.com/en-us/powershell/module/microsoftpowerbimgmt.profile/invoke-powerbirestmethod?view=powerbi-ps \